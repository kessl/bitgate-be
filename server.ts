import express from 'express'
import * as jobs from './src/mocks'

const version = 'v1'

export const app = express()
app.use(express.json())

app.get(`/${version}/queue`, (req, res) => res.json([jobs.waiting1, jobs.waiting2]))
app.post(`/${version}/queue`, (req, res) => res.json(jobs.waiting1))
app.get(`/${version}/history`, (req, res) => res.json([jobs.done, jobs.timedout]))
app.get(`/${version}/suggested`, (req, res) => res.json([jobs.done]))
app.get(`/${version}/position`, (req, res) => {
  if (!Array.isArray(req.body.uids)) return res.status(400).send('Missing parameter: uids[]')
  return res.json(
    req.body.uids.map((uid: string) => {
      const job = Object.values(jobs).find(job => job.status === 'WAITING' && job.uid === uid)
      return {
        uid,
        position: job ? job.position : null,
      }
    })
  )
})
app.post(`/${version}/result`, (req, res) => res.status(501).send())
