# bitgate API

Mock API for now. The real thing is going to keep track of a job queue and feed the jobs to an 8-bit breadboard CPU.

[![pipeline status](https://gitlab.com/kessl/bitgate-be/badges/master/pipeline.svg)](https://gitlab.com/kessl/bitgate-be/pipelines)
![coverage report](https://gitlab.com/kessl/bitgate-be/badges/master/coverage.svg)
