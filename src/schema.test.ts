import { OpenApiValidator } from 'express-openapi-validate'
import fs from 'fs'
import yaml from 'js-yaml'
import request from 'supertest'

import { app } from '../server'

const schema = yaml.safeLoad(fs.readFileSync('bitgate.openapi.yml', 'utf-8'))
const validator = new OpenApiValidator(schema)
const version = 'v1'

const endpoints = Object.entries(schema.paths)
  .map(row =>
    Object.entries(row[1]).map(method => ({
      ...method[1],
      url: row[0],
      method: method[0],
    }))
  )
  .reduce((acc, val) => acc.concat(val), [])

let i = 1
const mock = (schema: any): any => {
  switch (schema.type) {
    case 'integer':
      return i++
    case 'string':
      return 'dummy string'
    case 'array':
      return [mock(schema.items), mock(schema.items)]
  }
}

const makeRequest = (endpoint: any): request.Test => {
  if (endpoint.method === 'get') {
    return request(app).get(`/${version}${endpoint.url}`)
  }
  if (endpoint.method === 'post') {
    return request(app).post(`/${version}${endpoint.url}`)
  }
  throw 'Unknown request method'
}

endpoints.forEach(endpoint => {
  describe(`${endpoint.method} ${endpoint.url}`, () => {
    const validateResponse = validator.validateResponse(endpoint.method, endpoint.url)

    it('returns valid response', async () => {
      const requestBody = endpoint.requestBody ? mock(endpoint.requestBody.content['application/json'].schema) : {}
      const res = await makeRequest(endpoint).send(requestBody)
      expect(validateResponse(res)).toBeUndefined()
    })

    if (endpoint.requestBody) {
      it('return defined response on invalid request parameters', async () => {
        const res = await makeRequest(endpoint).send({})
        expect(validateResponse(res)).toBeUndefined()
      })
    }
  })
})
